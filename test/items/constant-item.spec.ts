import { ConstantItem } from '~/app/items/constant-item';

describe('VeryChic Constant Item', () => {
  it('should not update quality', () => {
    let item: ConstantItem = new ConstantItem({ name: 'Constant item', sellIn: 10, quality: 80 });
    item.update();
    expect(item.quality).toBe(80);
  });

  it('should not update sellin date', () => {
    let item: ConstantItem = new ConstantItem({ name: 'Constant item', sellIn: 10, quality: 80 });
    item.update();
    expect(item.sellIn).toBe(10);
  });
});