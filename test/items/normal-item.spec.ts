import { NormalItem } from '~/app/items/normal-item';

describe('VeryChic Normal Item', () => {
  describe('getters', () => {
    it('should return item name', () => {
      const name = 'Normal item';
      const item: NormalItem = new NormalItem({ name, sellIn: 0, quality: 10 });
      expect(item.name).toBe(name);
    });

    it('should return item sellIn', () => {
      const sellIn = 10;
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn, quality: 10 });
      expect(item.sellIn).toBe(sellIn);
    });

    it('should return item quality', () => {
      const quality = 10;
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn: 0, quality });
      expect(item.quality).toBe(quality);
    });
  });

  describe('setters', () => {
    it('should set original item\'s name', () => {
      const item = { name: 'Original name', sellIn: 10, quality: 20 };
      const updatedName = 'Updated name';
      const wrapper = new NormalItem(item);
      wrapper.name = updatedName;
      expect(item.name).toBe(updatedName);
    });

    it('should set original item\'s sellin', () => {
      const item = { name: 'Original name', sellIn: 10, quality: 20 };
      const updatedSellin = 50;
      const wrapper = new NormalItem(item);
      wrapper.sellIn = updatedSellin;
      expect(item.sellIn).toBe(updatedSellin);
    });

    it('should set original item\'s quality', () => {
      const item = { name: 'Original name', sellIn: 10, quality: 20 };
      const updatedQuality = 50;
      const wrapper = new NormalItem(item);
      wrapper.quality = updatedQuality;
      expect(item.quality).toBe(updatedQuality);
    });
  });

  describe('quality update factor', () => {
    it('should use a factor of -1 before sell by date', () => {
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn: 0, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(-1);
    });

    it('should use a factor of -2 after the sell by date', () => {
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn: -1, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(-2);
    });
  });

  describe('quality update', () => {
    it('should not reduce quality below 0', () => {
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn: 0, quality: 0 });
      const fn = jest.spyOn(item, 'qualityUpdateFactor', 'get').mockReturnValue(-1);
      item.update();
      expect(fn).toHaveBeenCalledTimes(1);
      expect(item.quality).toBe(0);
    });

    it('should not increase the quality above 50', () => {
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn: 0, quality: 49 });
      const fn = jest.spyOn(item, 'qualityUpdateFactor', 'get').mockReturnValue(1);
      item.update();
      item.update();
      expect(fn).toHaveBeenCalledTimes(2);
      expect(item.quality).toBe(50);
    });
  });

  describe('sell by', () => {

    it('should reduce sell by date by 1', () => {
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn: 10, quality: 0 });
      item.update();
      expect(item.sellIn).toBe(9);
    });

    it('should continue reducing sell by date even below 0', () => {
      const item: NormalItem = new NormalItem({ name: 'Normal item', sellIn: 0, quality: 0 });
      item.update();
      expect(item.sellIn).toBe(-1);
    });
  });
})