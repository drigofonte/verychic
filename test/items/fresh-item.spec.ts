import { FreshItem } from '~/app/items/fresh-item';

describe('VeryChic Constant Item', () => {
  describe('degrade quality before sell by date', () => {
    
    it('should reduce quality by 2', () => {
      const item: FreshItem = new FreshItem({ name: 'Fresh item', sellIn: 2, quality: 10 });
      item.update();
      expect(item.quality).toBe(8);
    });
  });

  describe('degrade quality after sell by date', () => {

    it('should reduce quality by 4', () => {
      const item: FreshItem = new FreshItem({ name: 'Normal item', sellIn: 0, quality: 10 });
      item.update();
      expect(item.quality).toBe(6);
    });
  });
});