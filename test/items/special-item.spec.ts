import { SpecialItem } from '~/app/items/special-item';

describe('VeryChic Constant Item', () => {
  describe('quality update factor', () => {
    it('should be 1 before sell by', () => {
      const item: SpecialItem = new SpecialItem({ name: 'Vintage item', sellIn: 0, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(1);
    });

    it('should be 1 after sell by', () => {
      const item: SpecialItem = new SpecialItem({ name: 'Vintage item', sellIn: -1, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(1);
    });
  });

  describe('quality update', () => {
    it('should increase quality by 1', () => {
      const item: SpecialItem = new SpecialItem({ name: 'Vintage item', sellIn: -1, quality: 10 });
      item.update();
      expect(item.quality).toBe(11);
    });
  });
});