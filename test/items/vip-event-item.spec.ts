import { VIPEventItem } from '~/app/items/vip-event-item';

describe('VeryChic Constant Item', () => {
  describe('quality update factor', () => {
    it('should be (2) 10 days or less before the sell by', () => {
      const item: VIPEventItem = new VIPEventItem({ name: 'VIP event item', sellIn: 10, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(2);
    });

    it('should be (3) 5 days or less before the sell by', () => {
      const item: VIPEventItem = new VIPEventItem({ name: 'VIP event item', sellIn: 5, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(3);
    });

    it('should be (quality * -1) after on the sell by', () => {
      const item: VIPEventItem = new VIPEventItem({ name: 'VIP event item', sellIn: 0, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(-10);
    });

    it('should be (quality * -1) after the sell by', () => {
      const item: VIPEventItem = new VIPEventItem({ name: 'VIP event item', sellIn: -1, quality: 10 });
      expect(item.qualityUpdateFactor).toBe(-10);
    });
  });
});