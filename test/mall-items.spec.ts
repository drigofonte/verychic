import { ConstantItem, FreshItem, NormalItem, SpecialItem, VIPEventItem } from "~/app/items";
import { Item } from "~/app/mall-inventory";
import { MallItems } from "~/app/mall-items";

describe('VeryChic Mall Items', () => {
  describe('fresh items', () => {
    it('should return an instance of a fresh fish item', () => {
      const item = new Item('Fresh fish', 0, 0);
      const converted = MallItems.toUpdatableItem(item);
      expect(converted instanceof FreshItem).toBeTruthy();
    });

    it('should return an instance of a fresh apple item', () => {
      const item = new Item('Fresh apple', 0, 0);
      const converted = MallItems.toUpdatableItem(item);
      expect(converted instanceof FreshItem).toBeTruthy();
    });
  });

  describe('normal items', () => {
    it('should return an instance of a normal item', () => {
      const item = new Item('Coffee', 0, 0);
      const converted = MallItems.toUpdatableItem(item);
      expect(converted instanceof NormalItem).toBeTruthy();
    });
  });

  describe('constant items', () => {
    it('should return an instance of a constant item', () => {
      const item = new Item('Millenary Honey', 0, 0);
      const converted = MallItems.toUpdatableItem(item);
      expect(converted instanceof ConstantItem).toBeTruthy();
    });
  });

  describe('special items', () => {
    it('should return an instance of a special item', () => {
      const item = new Item('Aged Cheese', 0, 0);
      const converted = MallItems.toUpdatableItem(item);
      expect(converted instanceof SpecialItem).toBeTruthy();
    });
  });

  describe('vip event items', () => {
    it('should return an instance of a vip event item', () => {
      const item = new Item('VIP event', 0, 0);
      const converted = MallItems.toUpdatableItem(item);
      expect(converted instanceof VIPEventItem).toBeTruthy();
    });
  });
});