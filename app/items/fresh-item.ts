import { NormalItem } from "./normal-item";

export class FreshItem extends NormalItem {
  override get qualityUpdateFactor(): number {
    return this.sellIn <= 0 ? -4 : -2;
  }
}