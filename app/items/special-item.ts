import { NormalItem } from './normal-item';

export class SpecialItem extends NormalItem {
  override get qualityUpdateFactor(): number {
    return 1;
  }
}