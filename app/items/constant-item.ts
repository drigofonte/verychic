import { NormalItem } from './normal-item';

export class ConstantItem extends NormalItem {

  override updateQuality(): void {
    // Do nothing
  }

  override updateSellIn(): void {
    // Do nothing
  }
}