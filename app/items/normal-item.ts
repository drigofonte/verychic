import { Item } from './item';
import { UpdatableItem } from './updatable-item';

export class NormalItem implements UpdatableItem {
  private readonly minQuality: number = 0;
  private readonly maxQuality: number = 50;

  constructor(public item: Item) { }

  public get name(): string {
    return this.item.name;
  }

  public set name(val: string) {
    this.item.name = val;
  }

  public get sellIn(): number {
    return this.item.sellIn;
  }

  public set sellIn(val: number) {
    this.item.sellIn = val;
  }

  public get quality(): number {
    return this.item.quality;
  }

  public set quality(val: number) {
    this.item.quality = val;
  }

  public get qualityUpdateFactor(): number {
    return this.sellIn < 0 ? -2 : -1;
  }

  protected updateQuality(): void {
    const factor = this.qualityUpdateFactor;
    this.quality = Math.max(this.quality + factor, this.minQuality);
    this.quality = Math.min(this.quality, this.maxQuality);
  }

  protected updateSellIn(): void {
    this.sellIn -= 1;
  }

  public update(): void {
    this.updateSellIn();
    this.updateQuality();
  }
}