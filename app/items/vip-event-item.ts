import { SpecialItem } from './special-item';

export class VIPEventItem extends SpecialItem {
  override get qualityUpdateFactor(): number {
    let factor = super.qualityUpdateFactor;
    if (this.sellIn <= 0) {
      factor = this.quality * -1;
    } else if (this.sellIn <= 5) {
      factor = 3;
    } else if (this.sellIn <= 10) {
      factor = 2;
    }
    return factor;
  }
}