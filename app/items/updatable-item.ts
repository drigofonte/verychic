import { Item } from "./item";

export interface UpdatableItem extends Item {
  item: Item;

  get qualityUpdateFactor(): number
  update(): void;
}