import { ConstantItem, FreshItem, NormalItem, SpecialItem, VIPEventItem } from "./items";
import { Item } from "./items/item";
import { UpdatableItem } from "./items/updatable-item";

export class MallItems {

  private static toSpecificUpdatableItem(val: Item): UpdatableItem | undefined {
    const { name } = val;
    let item: UpdatableItem | undefined;
    const lowerCaseName = name.toLowerCase();
    switch(lowerCaseName) {
      case 'aged cheese':
        item = new SpecialItem(val);
        break;
      case 'millenary honey':
        item = new ConstantItem(val);
        break;
    }

    return item;
  }

  public static toUpdatableItem(val: Item): UpdatableItem {
    let item = MallItems.toSpecificUpdatableItem(val);
    if (!item) {
      const { name } = val;
      const lowercaseName = name.toLowerCase();
      if (lowercaseName.includes('vip')) {
        item = new VIPEventItem(val);
      } else if (lowercaseName.includes('fresh')) {
        item = new FreshItem(val);
      } else {
        item = new NormalItem(val);
      }
    }
    return item;
  }
}